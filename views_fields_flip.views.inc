<?php

/**
 * @file
 * Provide an accordion style plugin for Views. This file is autoloaded by views.
 */

/**
 * Implementation of hook_views_plugin().
 */
function views_fields_flip_views_plugins() {
  return array(
    'style' => array(
      'views_fields_flip' => array(
        'title' => t('Views Fields Flip'),
        'theme' => 'views_view_fields_flip',
        'help' => t('Blaublau'),
        'handler' => 'views_fields_flip_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
