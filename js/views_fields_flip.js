Drupal.behaviors.views_fields_flip = function(context) {  
    if(Drupal.settings.views_fields_flip){    
        $.each(Drupal.settings.views_fields_flip, function(id) {
            var activeClass = 'flield-flip-header-active'; // the CSS class that the active header will recieve when it's open
            var hoverClass = 'flield-flip-header-hover'; // the CSS class that the headers will recieve when the mouse goes over
            var contentClass = 'flield-flip-content'; // the CSS class that the content in the accordions will have
            /*
             * Our view settings
             */
            var usegroupheader = this.usegroupheader;
            var grouped = this.grouping;  // wether or not field grouping is enabled
            var speed = this.speed;  // how fast the sliding will be
            var direction = this.direction;
            var color = this.color;
            //console.log('usegruopheader :'+usegroupheader+' grouped '+grouped+' speed '+speed);
            // the selectors we have to play with
            var contentSelector = 'div.' + contentClass; // used for selecting all accordion content to show/hide
            var displaySelector = this.display;  // Used to grab anything under our view.
            var headerSelector = usegroupheader ? (this.header) : ('.' + this.header); // this.header is the class of our first field
      
            // we have to use different selectors if using field grouping because we have several divs with #id
            var idSelector = grouped ? ('.' + id) : ('#' + id);      
            var $viewcontent = $('#' + displaySelector);
      
            var $triggers = usegroupheader ? $(headerSelector, $viewcontent) : $(idSelector + ' .views-fields-flip-item ' + headerSelector);
            $triggers.addClass('fields-flip-header');
           
            $triggers.parent().each(function(){
                // we wrap all but the header in a div so we can target the content later
                $(this).children().slice(1).wrapAll('<div class="' + contentClass + '">') 
            }).parent().addClass('flip-active');      
              
            var $content =  usegroupheader ? $(contentSelector, $viewcontent) : $(idSelector + ' ' + contentSelector);
            $content.hide();
                     
            $triggers.click(function(ev) {
        
                if (ev.detail === 1 || !ev.detail) { // so we prevent double clicking madness (for not so savy web users) !ev.detail is for when its triggered by code
          
                    var elem = $(this);
                    
                    if(elem.data('flipped')){            
                        elem.revertFlip();
                        elem.data('flipped',false);              
                    }else {                        
                        elem.flip({
                            direction: direction,
                            speed: speed,
                            color:color,
                            content:elem.parent().children().slice(1),	            
                        });	          
                        elem.data('flipped',true);	                    
                    }              
                }       
            });   
        });
    }
};