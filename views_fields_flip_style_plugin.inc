<?php

/**
 * @file
 * Provide an flip style plugin for views. This file is autoloaded by views.
 * 
 */

/**
 * Implementation of views_plugin_style().
 */
class views_fields_flip_style_plugin extends views_plugin_style {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['use-grouping-header'] = array('default' => 0);
    $options['speed'] = array('default' => 350);
    $options['row-start-open'] = array('default' => 0);
    $options['include-style'] = array('default' => 1);
    $options['flip-direction'] = array('default' => 'lr');
    $options['color'] = array('default' => '#');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['grouping']['#prefix'] = '<div class="form-item">' . t('<strong>IMPORTANT:</strong> The <em>first field</em> in order of appearance <em>will</em> be the one used as the "header" or "trigger" of the accordion action.') . '</div>';
    // available valid options for grouping (used for use-grouping-header #dependency)
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      $options[] = $field;
    }
    $form['use-grouping-header'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the group header as the Flip element'),
      '#default_value' => $this->options['use-grouping-header'],
      '#description' => t('If checked, the Group\'s header will be used to do the flip effect.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-style-options-grouping' => $options),
    );
    $form['speed'] = array(
      '#type' => 'textfield',
      '#title' => t('Transition time'),
      '#size' => 5,
      '#maxlength' => 5,
      '#default_value' => $this->options['speed'],
      '#field_suffix' => t('Miliseconds'),
      '#required' => TRUE,
      '#element_validate' => array('flip_speed_validate'),
      '#description' => t('The time in miliseconds to flip elements. The value <em>must be higher than 0</em>. If you would like it to be instanteneous, enter 0.001. Do not use commas in this field.'),
    );
    $form['include-style'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use the module's default styling"),
      '#default_value' => $this->options['include-style'],
      '#description' => t("If you disable this, the file in the module's directory <em>views-accordion.css</em> will not be loaded"),
    );
    $form['flip-direction'] = array(
      '#type' => 'select',
      '#title' => t("Direction for the flip efect"),
      '#default_value' => $this->options['flip-direction'],
      '#description' => t("Texto de ayuda"),
      '#options' => array(
        'tb' => 'Top-Bottom',
        'bt' => 'Bottom-Top',
        'lr' => 'Left-Right',
        'rl' => 'Right-Left',
      ),
    );
    $form['color'] = array(
      '#type' => 'textfield',
      '#title' => t("Background color for do flip efect"),
      '#default_value' => $this->options['color'],
      '#description' => t("Please , select your background for do flip efect. For example #FFFFFF"),
      '#size' => 10,
      '#element_validate' => array('fields_flip_color_validate'),
    );
  }

  /**
   * Render the display in this style.
   */
  function render() {
    $output = parent::render();
    drupal_add_js(drupal_get_path('module', 'views_fields_flip') . '/js/jquery-1.6.3.js');
    drupal_add_js(drupal_get_path('module', 'views_fields_flip') . '/js/jquery_flip/jquery.flip.js');
    drupal_add_js(drupal_get_path('module', 'views_fields_flip') . '/js/views_fields_flip.js');

    $view_settings = array();

    // Default settings:
    $view_settings['grouping'] = $this->options['grouping'] ? 1 : 0;
    $view_settings['usegroupheader'] = $view_settings['grouping'] ? $this->options['use-grouping-header'] : 0;
    $view_settings['speed'] = check_plain($this->options['speed']);
    $view_settings['direction'] = $this->options['flip-direction'];
    $view_settings['color'] = $this->options['color'];
    if ($view_settings['usegroupheader'] == 1) {
      $view_settings['header'] = 'h3.' . $view_id;
    }
    if ($view_settings['usegroupheader'] == 0) {
      $i = 0;
      foreach ($this->view->field as $id => $value) {
        if (($i == 0) && ($value->options['exclude'] == 0)) {
          $view_settings['header'] = 'views-field-' . str_replace('_', '-', $id);
          $i++;
        }
      }
    }

    $view_id = 'views-fields-flip-' . $this->view->name . '-' . $this->view->current_display;
    //We add the js settings:
    drupal_add_js(array('views_fields_flip' => array($view_id => $view_settings)), 'setting');

    if ($this->options['include-style']) {
      //Add the css file:
      drupal_add_css(drupal_get_path('module', 'views_fields_flip') . '/views_fields_flip.css');
    }
    return $output;
  }

}

/**
 * Validates the options entered
 */
function flip_speed_validate($element, &$form_state) {
  /* must be numeric value */
  if (!is_numeric($element['#value'])) {
    form_error($element, t('<em> @element </em> must be a numeric value. Use "." instance of "," with decimal numbers, for example 6 or 1.02.', array('@element' => $element['#title'])));
  }

  /* must be higher than 0 */
  if ($element['#value'] <= 0) {
    form_error($element, t('<em> @element must be a number higher than 0. If you would like it to be instantaneous, enter 0.001', array('@element' => $element['#title'])));
  }
}

/**
 * Color element validation.
 */
function fields_flip_color_validate($element, &$form_state) {
  //Validate #fff or #ffffff format  
  if (!preg_match('/^#([0-9 a-f A-F]){6}$/', $element['#value']) && !preg_match('/^#([0-9 a-f A-F]){3}$/', $element['#value'])) {
    form_error($element, t('The @element must be a hexadecimal number, for example #fff or #2c2c2c', array("@element" => $element['#title'])));
  }
}
